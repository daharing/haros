## DESCRIPTION
This project is focused on the development of my own bootloader and kernel together creating an operating system. I have written it all from scratch and am actively developing them. I welcome anyone that is interested in helping. 

## Setup
Run `make setup` to install and setup everything needed to build the operating system. This will take 10 to 30 minutes depending on your machine.

## Build
Run `make` or `make build` to build the operating system.

## Test
Run `make test` to run the OS in a virtual machine (VM).

## Burn
Run `make burn` to burn the OS to a bootable device such as a flash drive.

## Clean
To cleanup the file system, but not remove the compiled OS, run `make clean`. To clean everything, run `make clean-all`


---

** *The commands below should only be run if you know what you're doing* **

---

## Dependencies
Run `make dependencies` to download and install all programs that are needed.


## Source Code
Run `make download` to download all the source code needed. This would be the gcc and binutils source code.

## Installation
Run `make install` to install anything not from the repos (the compilers, gcc and binutils, in this case).

## Create ISO
Run `make iso` Create the bootable ISO file from the bootloader/grub and the HarOS.bin file.
