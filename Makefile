# Author:		Dustin Haring
# Date Created:	March 28, 2019
# Resources: 	http://www.gnu.org/software/make/manual/make.html
# Desc:			This makefile is used to setup, build, and test the operating system project.

## Parameters ##
TARGET=i686-elf
##						##

#target … : prerequisites …
#        recipe

# Where
#		target -> name of file generated
#		prerequisites ->  a file that is used as input to create the target
#		recipe -> actions taken by make. Tab is required.

.PHONY: build burn test test_kernel test_grub setup dependencies download install clean clean-all

# Build project
build:
	$(MAKE) -s -C src

# Write image to disk
burn: 
	./burnToDevice.sh

# Test built project
test: HarOS.bin
	$(MAKE) test -s -C src

# This doesn't work. Need to implement fix FIXME 
iso: HarOS.bin

	dd status=noxfer conv=notrunc if=HarOS.bin of=HarOS.flp bs=10k count=144 
	mkdir -p iso 
	cp HarOS.flp iso/ 
	mkisofs -r -no-emul-boot -boot-load-size 4 -boot-info-table -V 'HARINGOS' -o iso/HarOS.iso -b HarOS.flp iso/

	# Ensure iso directory exists, create it otherwise
	mkdir -p iso

	# Create .flp image from the HarOS.bin (bootloader binary)
	echo ">>> Creating floppy image from OS binaries..."	
	dd status=noxfer conv=notrunc if=HarOS.bin of=iso/HarOS.flp 

	echo ">>> Creating ISO image from floppy image..."	
	mkisofs -no-emul-boot -boot-load-size 4 -boot-info-table -V 'HARINGOS' -o iso/HarOS.iso -b HarOS.flp iso/
	qemu-system-i386 -boot d -cdrom iso/HarOS.iso -m 512

	# Cleanup
	rm -rf iso

	echo ">>> Finished!"

# TODO Implement ability to use grub bootloader
#test_grub: iso 
#	make iso
#	## https://wiki.osdev.org/Bare_Bones section "Booting the Kernel"
#
#	# Dependencies
#	#sudo apt install qemu-system-i386
#
#	# Use the iso as a cdrom on the VM
#	qemu-system-i386 -cdrom HarOS.iso
#
#	## Alternatively, boot the kernel without a bootloader nor the iso or linker
#	#qemu-system-i386 -kernel ../HarOS.bin

# TODO Implement ability to only run kernel
#test_kernel: src/kernel.bin
#	# Dependencies
#	#sudo apt install qemu-system-i386
#
#	## Use the iso as a cdrom on the VM
#	#qemu-system-i386 -cdrom HarOS.iso
#
#	# Alternatively, boot the kernel without a bootloader nor the iso
#	qemu-system-i386 -kernel src/kernel.bin

#iso: src/kernel.bin
#	## https://wiki.osdev.org/Bare_Bones section "Booting the Kernel"
#
#	mkdir -p isodir/boot/grub
#
#	cp src/kernel.bin isodir/boot/kernel.bin
#	cp src/Kernel/grub.cfg isodir/boot/grub/grub.cfg
#	grub-mkrescue -o grub_HarOS.iso isodir 
#
#	# Cleanup
#	rm -R isodir

# Fully setup all aspects nescessary to build the project
setup:
	make dependencies
	make download
	make install

# Fetch and Install Dependency Software
dependencies:
	sudo apt install qemu-system-i386 xorriso

	$(MAKE) dependencies -s -C Compilers
	$(MAKE) dependencies -s -C src

# Download any items possible (such as source codes)
download:
	$(MAKE) download -s -C Compilers
	$(MAKE) download -s -C src

# Build/install local tools needed to build project (ie cross-compilers)
install:
	$(MAKE) install -s -C Compilers
	$(MAKE) install -s -C src

# Clean unnecessary files but keep essiential files
clean:
	$(MAKE) clean -s -C Compilers
	$(MAKE) clean -s -C src

# Clean all generated files
clean-all:
	$(MAKE) clean-all -s -C Compilers
	$(MAKE) clean-all -s -C src
	rm -rf HarOS.iso
