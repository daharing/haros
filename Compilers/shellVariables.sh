#!/usr/bin/env bash

echo "TO RUN THIS SCRIPT: BE SURE TO USE \"source ./shellVariables.sh\" or \". ./shellVariables.sh\" commands"

#https://www.digitalocean.com/community/tutorials/how-to-read-and-set-environmental-and-shell-variables-on-a-linux-vps

if [ -z "$1" ]; then
  echo "Defaulting to i686-elf architecture"
  TARGET=i686-elf
fi

# Setup Shell environment variables
#BASE_PATH=$(pwd) #Get working directory
BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" #Get absolute path to this script reguardless of working directory
export PREFIX="$BASE_PATH/opt/cross"
export TARGET=i686-elf #Command base, to use gcc, use i686-elf-gcc
export PATH="$PREFIX/bin:$PATH"
export PATH="$BASE_PATH/opt/cross/bin:$PATH"

#aAfter this script runs, in the same terminal you can use i686-elf-<program> instead of opt/cross/bin/<program>
#Because these are shell environment variables, they are not permanent and only apply to the current shell session,
#   hence why this script needs 'source' to run because source runs the script in current shell instead of in a child shell.
