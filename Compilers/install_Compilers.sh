#!/usr/bin/env bash

#https://wiki.osdev.org/GCC_Cross-Compiler
#https://www6.software.ibm.com/developerworks/education/l-cross/l-cross-ltr.pdf

###----------EDIT THESE----------###
NumOfThreads="8" # The -j $NumOfThreads is experimental on my part, but it is said that it will run make i$
###----------    END    ----------###


if [ -z "$1" ]; then
  echo "Defaulting to i686-elf architecture"
  TARGET=i686-elf #Target platform
else
  TARGET=$1
fi

if [ -z "$2" ]; then
  GCC_Version="gcc-8.3.0"
  echo "Defaulting to $GCC_Version"
else
  GCC_Version=$2
fi

if [ -z "$3" ]; then
  BINUTILS_Version="binutils-2.32"
  echo "Defaulting to $BINUTILS_Version"
else
  BINUTILS_Version=$3
fi

## Variables ##

# GCC and BinUtils source URLs
GCC_SOURCE="https://ftp.gnu.org/gnu/gcc/$GCC_Version/$GCC_Version.tar.xz"
BINUTILS="https://ftp.gnu.org/gnu/binutils/$BINUTILS_Version.tar.xz"

# Setup Path Variables:
BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" #Get absolute path to this s$
PREFIX="$BASE_PATH/opt/cross"

## Set Our Working Directory
echo "Working Dirctory is $BASE_PATH"
cd "$BASE_PATH"

## Create Folders ##
echo "Create Folders: $BASE_PATH/opt/cross"
mkdir -p "$BASE_PATH/opt/cross/"

## Dependencies ##
sudo apt update

# For binary utilities
sudo apt install automake build-essential

# For GCC
#sudo apt install libgmp3-dev libmpc-dev libmpfr-dev
sudo apt install libgmp10 libmpc-dev libmpfr-dev

## Download source files from the following
wget -c --no-clobber $GCC_SOURCE
wget -c --no-clobber $BINUTILS


# The binutils folder contains an automake file. Be sure to have automake installed. The commands below are used to 
# install the binutils to the project directory using automake ( ./configure ).

## Build the binary-utilities  ##
# some autoconfig notes: https://www.gnu.org/software/automake/manual/html_node/Cross_002dCompilation.html
tar xf "$BINUTILS_Version.tar.xz"
mkdir build-binutils
cd build-binutils
../$BINUTILS_Version/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make -j $NumOfThreads 
make install -j $NumOfThreads


## Build GCC ##

cd .. # move up out of the build-binutils directory
# The $PREFIX/bin dir _must_ be in the PATH. We did that above.
which -- $TARGET-as || echo "$TARGET-as is not in the PATH"
tar xf "$GCC_Version.tar.xz"
mkdir build-gcc
cd build-gcc
../$GCC_Version/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc -j $NumOfThreads
make all-target-libgcc -j $NumOfThreads
make install-gcc -j $NumOfThreads
make install-target-libgcc -j $NumOfThreads

## Test ##
echo "\n\n\n\n Test gcc by running $PREFIX/bin/$TARGET-gcc --version"
# To use the compilers, navigate to folder opt/cross/bin/$TARGET-<program> where <program> is the program you wish to access such as 'gcc' or 'as'.
$PREFIX/bin/$TARGET-gcc --version #should output the version of gcc as a test

echo
echo "To use compilers, use path Compilers/opt/cross/bin/$TARGET-<program> ."
echo "Example, opt/cross/bin/$TARGET-gcc"
echo "Otherwise, you can use shell environment variables to simplify the commands. Run \"source ./shellVariables.sh\", then you may simply type $TARGET-<program>. Example, $TARGET-gcc"
